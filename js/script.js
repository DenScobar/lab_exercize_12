/* Задания на урок:

1) Реализовать функционал, что после заполнения формы и нажатия кнопки "Подтвердить" - 
новый фильм добавляется в список. Страница не должна перезагружаться.
Новый фильм должен добавляться в movieDB.movies.
Для получения доступа к значению input - обращаемся к нему как input.value;
P.S. Здесь есть несколько вариантов решения задачи, принимается любой, но рабочий.

2) Если название фильма больше, чем 21 символ - обрезать его и добавить три точки

3) При клике на мусорную корзину - элемент будет удаляться из списка (сложно)

4) Если в форме стоит галочка "Сделать любимым" - в консоль вывести сообщение: 
"Добавляем любимый фильм"

5) Фильмы должны быть отсортированы по алфавиту */

'use strict';

const movieDB = {
    movies: [
        "Логан",
        "Лига справедливости",
        "Ла-ла лэнд",
        "Одержимость",
        "Скотт Пилигрим против...",
    ]
};

// Обращение к селекторам
const adv = document.querySelector('.promo__adv'),
    content = document.querySelector('.promo__content'),
    bg = content.querySelector('.promo__bg'),
    genre = bg.querySelector('.promo__genre'),
    films = content.getElementsByClassName('promo__interactive-item'),
    filmsUl = document.getElementsByClassName('promo__interactive-list'),
    btn = document.querySelector('button'),
    forma = document.forms,
    filmName = forma[1].inputfilm,
    filmCheck = forma[1].inputcheck;

const ruCollator = new Intl.Collator('ru-RU');

adv.remove();
content.style.width = 'calc(100% - 300px)';
genre.innerHTML = 'Драма';
bg.style.backgroundImage = 'url(img/bg.jpg)';


// Обнуление всего списка, чтобы очистить все li в html и добавить фильмы из массива
// Это стоило сделать так в предыдущем задании, но оставлю там так, как есть, пусть будет и предыдущий вариант
filmDelete();
filmInner();


btn.addEventListener('click', e => {
    e.preventDefault();
    let name = filmName.value;
    if (name) {
        if (name.length > 21) { //2) Если название фильма больше, чем 21 символ - обрезать его и добавить три точки
            name = `${name.substr(0, 21)}...`;
        }
        filmDelete();
        movieDB.movies.push(name);
        filmInner();
        filmName.value = '';
    }
});
// 4) Если в форме стоит галочка "Сделать любимым" - в консоль вывести сообщение: "Добавляем любимый фильм"
filmCheck.addEventListener('change', () => console.log('Добавляем любимый фильм'));

// 3) При клике на мусорную корзину - элемент будет удаляться из списка (сложно)
filmsUl[0].addEventListener("click", (event) => {
    if (event.target.closest('.delete')) {
        const title = event.target.closest('.delete').parentNode.textContent.substr(3);
        filmDelete();
        movieDB.movies.splice(movieDB.movies.indexOf(title), 1);
        filmInner();
    }
});

// Функция добавления элементов списка фильмов
function filmInner() {
    const films_sorted = movieDB.movies.sort((a, b) => ruCollator.compare(a, b));
    for (let item = films_sorted.length - 1; item >= 0; item--) {
        filmsUl[0].insertAdjacentHTML('afterbegin',
            `<li class="promo__interactive-item">${item + 1}. ${movieDB.movies[item]}<div class="delete"></div></li>`);
    }
}

// Функция удаления элементов списка фильмов
function filmDelete() {
    const films_sorted = movieDB.movies.sort((a, b) => ruCollator.compare(a, b));
    for (let item = films_sorted.length - 1; item >= 0; item--) {
        films[item].remove();
    }
}
